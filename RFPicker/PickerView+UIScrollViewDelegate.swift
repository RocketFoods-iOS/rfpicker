//
//  PickerView+UIScrollViewDelegate.swift
//  RFPicker
//
//  Created by Nikita Arutyunov on 3/2/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension PickerView: UIScrollViewDelegate {
    public func scrollViewWillBeginDragging(_: UIScrollView) {
        isScrolling = true
    }

    public func scrollViewWillEndDragging(_: UIScrollView, withVelocity _: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let partialRow = Float(targetContentOffset.pointee.y / rowHeight) // Get the estimative of what row will be the selected when the scroll animation ends.
        var roundedRow = Int(lroundf(partialRow)) // Round the estimative to a row

        if roundedRow < 0 {
            roundedRow = 0
        } else {
            targetContentOffset.pointee.y = CGFloat(roundedRow) * rowHeight // Set the targetContentOffset (where the scrolling position will be when the animation ends) to a rounded value.
        }

        // Update the currentSelectedRow and notify the delegate that we have a new selected row.
        currentSelectedRow = indexForRow(roundedRow)

        delegate?.pickerView?(self, didSelectRow: currentSelectedRow)
    }

    public func scrollViewDidEndDecelerating(_: UIScrollView) {
        // When the orientation changes during the scroll, is required to reset the picker to select the nearby to middle row.
        if orientationChanged {
            selectedNearbyToMiddleRow(currentSelectedRow)
            orientationChanged = false
        }

        isScrolling = false
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let partialRow = Float(scrollView.contentOffset.y / rowHeight)
        let roundedRow = Int(lroundf(partialRow))

        // Avoid to have two highlighted rows at the same time
        if let visibleRows = tableView.indexPathsForVisibleRows {
            for indexPath in visibleRows {
                if let cellToUnhighlight = tableView.cellForRow(at: indexPath) as? Cell, (indexPath as NSIndexPath).row != roundedRow {
                    _ = delegate?.pickerView?(self, viewForRow: indexForRow((indexPath as NSIndexPath).row), highlighted: false, reusingView: cellToUnhighlight.customView)
                    delegate?.pickerView?(self, styleForLabel: cellToUnhighlight.titleLabel, highlighted: false)
                }
            }
        }

        // Highlight the current selected cell during scroll
        if let cellToHighlight = tableView.cellForRow(at: IndexPath(row: roundedRow, section: 0)) as? Cell {
            _ = delegate?.pickerView?(self, viewForRow: indexForRow(roundedRow), highlighted: true, reusingView: cellToHighlight.customView)
            _ = delegate?.pickerView?(self, styleForLabel: cellToHighlight.titleLabel, highlighted: true)
        }
    }
}
