//
//  PickerView+Cell.swift
//  RFPicker
//
//  Created by Nikita Arutyunov on 3/2/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension PickerView {
    class Cell: UITableViewCell {
        lazy var titleLabel: UILabel = {
            let titleLabel = UILabel(frame: CGRect(
                x: 0.0,
                y: 0.0,
                width: self.contentView.frame.width,
                height: self.contentView.frame.height
            ))
            
            titleLabel.textAlignment = .center

            return titleLabel
        }()

        var customView: UIView?
    }
}
