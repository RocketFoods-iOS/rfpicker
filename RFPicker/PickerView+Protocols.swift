//
//  PickerView+Protocols.swift
//  RFPicker
//
//  Created by Nikita Arutyunov on 3/2/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

@objc public protocol PickerViewDataSource: AnyObject {
    func pickerViewNumberOfRows(_ pickerView: PickerView) -> Int
    func pickerView(_ pickerView: PickerView, titleForRow row: Int) -> String
}

@objc public protocol PickerViewDelegate: AnyObject {
    func pickerViewHeightForRows(_ pickerView: PickerView) -> CGFloat
    @objc optional func pickerView(_ pickerView: PickerView, didSelectRow row: Int)
    @objc optional func pickerView(_ pickerView: PickerView, didTapRow row: Int)
    @objc optional func pickerView(_ pickerView: PickerView, styleForLabel label: UILabel, highlighted: Bool)
    @objc optional func pickerView(_ pickerView: PickerView, viewForRow row: Int, highlighted: Bool, reusingView view: UIView?) -> UIView?
}
