//
//  PickerView+UITableViewDelegate.swift
//  RFPicker
//
//  Created by Nikita Arutyunov on 3/2/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension PickerView: UITableViewDelegate {
    public func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectTappedRow((indexPath as NSIndexPath).row)
    }

    public func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let numberOfRowsInPickerView = (dataSource?.pickerViewNumberOfRows(self) ?? 0) * infinityRowsMultiplier

        // When the scrolling reach the end on top/bottom we need to set the first/last row to appear in the center of PickerView, so that row must be bigger.
        if (indexPath as NSIndexPath).row == 0 {
            return (frame.height / 2) + (rowHeight / 2)
        } else if numberOfRowsInPickerView > 0, (indexPath as NSIndexPath).row == numberOfRowsInPickerView - 1 {
            return (frame.height / 2) + (rowHeight / 2)
        }

        return rowHeight
    }
}
