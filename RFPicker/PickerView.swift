//
//  PickerView.swift
//  RFPicker
//
//  Created by Nikita Arutyunov on 08/04/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

// swiftlint:disable all

import UIKit

open class PickerView: UIView {
    var enabled = true {
        didSet {
            if enabled {
                turnPickerViewOn()
            } else {
                turnPickerViewOff()
            }
        }
    }

    var selectionOverlayH: NSLayoutConstraint!
    var selectionImageH: NSLayoutConstraint!
    var selectionIndicatorB: NSLayoutConstraint!
    
    var pickerCellBackgroundColor: UIColor?

    var numberOfRowsByDataSource: Int {
        return dataSource?.pickerViewNumberOfRows(self) ?? 0
    }

    var indexesByDataSource: Int {
        return numberOfRowsByDataSource > 0 ? numberOfRowsByDataSource - 1 : numberOfRowsByDataSource
    }

    var rowHeight: CGFloat {
        return delegate?.pickerViewHeightForRows(self) ?? 0
    }

    open override var backgroundColor: UIColor? {
        didSet {
            tableView.backgroundColor = backgroundColor
            pickerCellBackgroundColor = backgroundColor
        }
    }

    let pickerViewCellIdentifier = "pickerViewCell"

    open weak var dataSource: PickerViewDataSource?
    open weak var delegate: PickerViewDelegate?

    open lazy var defaultSelectionIndicator: UIView = {
        let selectionIndicator = UIView()
        selectionIndicator.backgroundColor = self.tintColor
        selectionIndicator.alpha = 0.0

        return selectionIndicator
    }()

    open lazy var selectionOverlay: UIView = {
        let selectionOverlay = UIView()
        selectionOverlay.backgroundColor = self.tintColor
        selectionOverlay.alpha = 0.0

        return selectionOverlay
    }()

    open lazy var selectionImageView: UIImageView = {
        let selectionImageView = UIImageView()
        selectionImageView.alpha = 0.0

        return selectionImageView
    }()

    public lazy var tableView: UITableView = {
        let tableView = UITableView()

        return tableView
    }()

    var infinityRowsMultiplier: Int = 1
    var hasTouchedPickerViewYet = false
    open var currentSelectedRow: Int!
    open var currentSelectedIndex: Int {
        return indexForRow(currentSelectedRow)
    }

    var firstTimeOrientationChanged = true
    var orientationChanged = false
    var screenSize: CGSize = UIScreen.main.bounds.size
    var isScrolling = false
    var setupHasBeenDone = false
    var shouldSelectNearbyToMiddleRow = true

    open var scrollingStyle = ScrollingStyle.default {
        didSet {
            switch scrollingStyle {
            case .default:
                infinityRowsMultiplier = 1
            case .infinite:
                infinityRowsMultiplier = generateInfinityRowsMultiplier()
            }
        }
    }

    open var selectionStyle = SelectionStyle.none {
        didSet {
            setupSelectionViewsVisibility()
        }
    }

    // MARK: Initialization

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }

    // MARK: Subviews Setup

    func setup() {
        infinityRowsMultiplier = generateInfinityRowsMultiplier()

        // Setup subviews constraints and apperance
        translatesAutoresizingMaskIntoConstraints = false
        setupTableView()
        setupSelectionOverlay()
        setupSelectionImageView()
        setupDefaultSelectionIndicator()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()

        // This needs to be done after a delay - I am guessing it basically needs to be called once
        // the view is already displaying
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            // Some UI Adjustments we need to do after setting UITableView data source & delegate.
            self.adjustSelectionOverlayHeightConstraint()
        }
    }

    func setupTableView() {
        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.separatorColor = .none
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.scrollsToTop = false
        tableView.register(Cell.classForCoder(), forCellReuseIdentifier: pickerViewCellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(tableView)

        let tableViewH = NSLayoutConstraint(item: tableView, attribute: .height, relatedBy: .equal, toItem: self,
                                            attribute: .height, multiplier: 1, constant: 0)
        addConstraint(tableViewH)

        let tableViewW = NSLayoutConstraint(item: tableView, attribute: .width, relatedBy: .equal, toItem: self,
                                            attribute: .width, multiplier: 1, constant: 0)
        addConstraint(tableViewW)

        let tableViewL = NSLayoutConstraint(item: tableView, attribute: .leading, relatedBy: .equal, toItem: self,
                                            attribute: .leading, multiplier: 1, constant: 0)
        addConstraint(tableViewL)

        let tableViewTop = NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: self,
                                              attribute: .top, multiplier: 1, constant: 0)
        addConstraint(tableViewTop)

        let tableViewBottom = NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: self,
                                                 attribute: .bottom, multiplier: 1, constant: 0)
        addConstraint(tableViewBottom)

        let tableViewT = NSLayoutConstraint(item: tableView, attribute: .trailing, relatedBy: .equal, toItem: self,
                                            attribute: .trailing, multiplier: 1, constant: 0)
        addConstraint(tableViewT)
    }

    func setupSelectionViewsVisibility() {
        switch selectionStyle {
        case .defaultIndicator:
            defaultSelectionIndicator.alpha = 1.0
            selectionOverlay.alpha = 0.0
            selectionImageView.alpha = 0.0
        case .overlay:
            selectionOverlay.alpha = 0.25
            defaultSelectionIndicator.alpha = 0.0
            selectionImageView.alpha = 0.0
        case .image:
            selectionImageView.alpha = 1.0
            selectionOverlay.alpha = 0.0
            defaultSelectionIndicator.alpha = 0.0
        case .none:
            selectionOverlay.alpha = 0.0
            defaultSelectionIndicator.alpha = 0.0
            selectionImageView.alpha = 0.0
        }
    }

    func setupSelectionOverlay() {
        selectionOverlay.isUserInteractionEnabled = false
        selectionOverlay.translatesAutoresizingMaskIntoConstraints = false
        addSubview(selectionOverlay)

        selectionOverlayH = NSLayoutConstraint(item: selectionOverlay, attribute: .height, relatedBy: .equal, toItem: nil,
                                               attribute: .notAnAttribute, multiplier: 1, constant: rowHeight)
        addConstraint(selectionOverlayH)

        let selectionOverlayW = NSLayoutConstraint(item: selectionOverlay, attribute: .width, relatedBy: .equal, toItem: self,
                                                   attribute: .width, multiplier: 1, constant: 0)
        addConstraint(selectionOverlayW)

        let selectionOverlayL = NSLayoutConstraint(item: selectionOverlay, attribute: .leading, relatedBy: .equal, toItem: self,
                                                   attribute: .leading, multiplier: 1, constant: 0)
        addConstraint(selectionOverlayL)

        let selectionOverlayT = NSLayoutConstraint(item: selectionOverlay, attribute: .trailing, relatedBy: .equal, toItem: self,
                                                   attribute: .trailing, multiplier: 1, constant: 0)
        addConstraint(selectionOverlayT)

        let selectionOverlayY = NSLayoutConstraint(item: selectionOverlay, attribute: .centerY, relatedBy: .equal, toItem: self,
                                                   attribute: .centerY, multiplier: 1, constant: 0)
        addConstraint(selectionOverlayY)
    }

    func setupSelectionImageView() {
        selectionImageView.isUserInteractionEnabled = false
        selectionImageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(selectionImageView)

        selectionImageH = NSLayoutConstraint(item: selectionImageView, attribute: .height, relatedBy: .equal, toItem: nil,
                                             attribute: .notAnAttribute, multiplier: 1, constant: rowHeight)
        addConstraint(selectionImageH)

        let selectionImageW = NSLayoutConstraint(item: selectionImageView, attribute: .width, relatedBy: .equal, toItem: self,
                                                 attribute: .width, multiplier: 1, constant: 0)
        addConstraint(selectionImageW)

        let selectionImageL = NSLayoutConstraint(item: selectionImageView, attribute: .leading, relatedBy: .equal, toItem: self,
                                                 attribute: .leading, multiplier: 1, constant: 0)
        addConstraint(selectionImageL)

        let selectionImageT = NSLayoutConstraint(item: selectionImageView, attribute: .trailing, relatedBy: .equal, toItem: self,
                                                 attribute: .trailing, multiplier: 1, constant: 0)
        addConstraint(selectionImageT)

        let selectionImageY = NSLayoutConstraint(item: selectionImageView, attribute: .centerY, relatedBy: .equal, toItem: self,
                                                 attribute: .centerY, multiplier: 1, constant: 0)
        addConstraint(selectionImageY)
    }

    func setupDefaultSelectionIndicator() {
        defaultSelectionIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(defaultSelectionIndicator)

        let selectionIndicatorH = NSLayoutConstraint(item: defaultSelectionIndicator, attribute: .height, relatedBy: .equal, toItem: nil,
                                                     attribute: .notAnAttribute, multiplier: 1, constant: 2.0)
        addConstraint(selectionIndicatorH)

        let selectionIndicatorW = NSLayoutConstraint(item: defaultSelectionIndicator, attribute: .width, relatedBy: .equal,
                                                     toItem: self, attribute: .width, multiplier: 1, constant: 0)
        addConstraint(selectionIndicatorW)

        let selectionIndicatorL = NSLayoutConstraint(item: defaultSelectionIndicator, attribute: .leading, relatedBy: .equal,
                                                     toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        addConstraint(selectionIndicatorL)

        selectionIndicatorB = NSLayoutConstraint(item: defaultSelectionIndicator, attribute: .bottom, relatedBy: .equal,
                                                 toItem: self, attribute: .centerY, multiplier: 1, constant: rowHeight / 2)
        addConstraint(selectionIndicatorB)

        let selectionIndicatorT = NSLayoutConstraint(item: defaultSelectionIndicator, attribute: .trailing, relatedBy: .equal,
                                                     toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        addConstraint(selectionIndicatorT)
    }

    // MARK: Infinite Scrolling Helpers

    func generateInfinityRowsMultiplier() -> Int {
        if scrollingStyle == .default {
            return 1
        }

        if numberOfRowsByDataSource > 100 {
            return 100
        } else if numberOfRowsByDataSource < 100, numberOfRowsByDataSource > 50 {
            return 200
        } else if numberOfRowsByDataSource < 50, numberOfRowsByDataSource > 25 {
            return 400
        } else {
            return 800
        }
    }

    // MARK: Life Cycle

    open override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)

        if let _ = newWindow {
            NotificationCenter.default.addObserver(self, selector: #selector(PickerView.adjustCurrentSelectedAfterOrientationChanges),
                                                   name: UIDevice.orientationDidChangeNotification, object: nil)
        } else {
            NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()

        if !setupHasBeenDone {
            setup()
            setupHasBeenDone = true
        }
    }

    func adjustSelectionOverlayHeightConstraint() {
        if selectionOverlayH.constant != rowHeight || selectionImageH.constant != rowHeight || selectionIndicatorB.constant != (rowHeight / 2) {
            selectionOverlayH.constant = rowHeight
            selectionImageH.constant = rowHeight
            selectionIndicatorB.constant = -(rowHeight / 2)
            layoutIfNeeded()
        }
    }

    @objc func adjustCurrentSelectedAfterOrientationChanges() {
        guard screenSize != UIScreen.main.bounds.size else {
            return
        }

        screenSize = UIScreen.main.bounds.size

        setNeedsLayout()
        layoutIfNeeded()

        shouldSelectNearbyToMiddleRow = true

        if firstTimeOrientationChanged {
            firstTimeOrientationChanged = false
            return
        }

        if !isScrolling {
            return
        }

        orientationChanged = true
    }

    func indexForRow(_ row: Int) -> Int {
        return row % (numberOfRowsByDataSource > 0 ? numberOfRowsByDataSource : 1)
    }

    // MARK: - Actions

    /**
     Selects the nearby to middle row that matches with the provided index.

     - parameter row: A valid index provided by Data Source.
     */
    func selectedNearbyToMiddleRow(_ row: Int) {
        currentSelectedRow = row
        tableView.reloadData()

        // This line adjust the contentInset to UIEdgeInsetZero because when the PickerView are inside of a UIViewController
        // presented by a UINavigation controller, the tableView contentInset is affected.
        tableView.contentInset = UIEdgeInsets.zero

        let indexOfSelectedRow = visibleIndexOfSelectedRow()
        tableView.setContentOffset(CGPoint(x: 0.0, y: CGFloat(indexOfSelectedRow) * rowHeight), animated: false)

        delegate?.pickerView?(self, didSelectRow: currentSelectedRow)
        shouldSelectNearbyToMiddleRow = false
    }

    /**
     Selects literally the row with index that the user tapped.

     - parameter row: The row index that the user tapped, i.e. the Data Source index times the `infinityRowsMultiplier`.
     */
    func selectTappedRow(_ row: Int) {
        delegate?.pickerView?(self, didTapRow: indexForRow(row))
        selectRow(row, animated: true)
    }

    func turnPickerViewOn() {
        tableView.isScrollEnabled = true
        setupSelectionViewsVisibility()
    }

    func turnPickerViewOff() {
        tableView.isScrollEnabled = false
        selectionOverlay.alpha = 0.0
        defaultSelectionIndicator.alpha = 0.0
        selectionImageView.alpha = 0.0
    }

    /**
     This is an private helper that we use to reach the visible index of the current selected row.
     Because of we multiply the rows several times to create an Infinite Scrolling experience, the index of a visible selected row may
     not be the same as the index provided on Data Source.

     - returns: The visible index of current selected row.
     */
    func visibleIndexOfSelectedRow() -> Int {
        let middleMultiplier = scrollingStyle == .infinite ? (infinityRowsMultiplier / 2) : infinityRowsMultiplier
        let middleIndex = numberOfRowsByDataSource * middleMultiplier
        let indexForSelectedRow: Int

        if let _ = currentSelectedRow, scrollingStyle == .default, currentSelectedRow == 0 {
            indexForSelectedRow = 0
        } else if let _ = currentSelectedRow {
            indexForSelectedRow = middleIndex - (numberOfRowsByDataSource - currentSelectedRow)
        } else {
            let middleRow = Int(floor(Float(indexesByDataSource) / 2.0))
            indexForSelectedRow = middleIndex - (numberOfRowsByDataSource - middleRow)
        }

        return indexForSelectedRow
    }

    open func selectRow(_ row: Int, animated: Bool) {
        var finalRow = row

        if scrollingStyle == .infinite, row <= numberOfRowsByDataSource {
            let middleMultiplier = scrollingStyle == .infinite ? (infinityRowsMultiplier / 2) : infinityRowsMultiplier
            let middleIndex = numberOfRowsByDataSource * middleMultiplier
            finalRow = middleIndex - (numberOfRowsByDataSource - finalRow)
        }

        currentSelectedRow = finalRow

        delegate?.pickerView?(self, didSelectRow: indexForRow(currentSelectedRow))

        tableView.setContentOffset(CGPoint(x: 0.0, y: CGFloat(currentSelectedRow) * rowHeight), animated: animated)
    }

    open func reloadPickerView() {
        shouldSelectNearbyToMiddleRow = true
        tableView.reloadData()
    }
}
