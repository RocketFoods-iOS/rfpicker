//
//  PickerView+UITableViewDataSource.swift
//  RFPicker
//
//  Created by Nikita Arutyunov on 3/2/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension PickerView: UITableViewDataSource {
    public func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        let numberOfRows = numberOfRowsByDataSource * infinityRowsMultiplier

        // Select the nearby to middle row when it's needed (first run or orientation change)
        if shouldSelectNearbyToMiddleRow, numberOfRows > 0 {
            // Configure the PickerView to select the middle row when the orientation changes during scroll
            if isScrolling {
                let middleRow = Int(floor(Float(indexesByDataSource) / 2.0))
                selectedNearbyToMiddleRow(middleRow)
            } else {
                let rowToSelect = currentSelectedRow ?? Int(floor(Float(indexesByDataSource) / 2.0))

                selectedNearbyToMiddleRow(rowToSelect)
            }
        }

        // If PickerView have items to show set it as enabled otherwise set it as disabled
        if numberOfRows > 0 {
            turnPickerViewOn()
        } else {
            turnPickerViewOff()
        }

        return numberOfRows
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let indexOfSelectedRow = visibleIndexOfSelectedRow()

        guard let pickerViewCell = tableView.dequeueReusableCell(
            withIdentifier: pickerViewCellIdentifier,
            for: indexPath
        ) as? Cell else { return UITableViewCell() }

        let view = delegate?.pickerView?(self, viewForRow: indexForRow((indexPath as NSIndexPath).row), highlighted: (indexPath as NSIndexPath).row == indexOfSelectedRow, reusingView: pickerViewCell.customView)

        pickerViewCell.selectionStyle = .none
        pickerViewCell.backgroundColor = pickerCellBackgroundColor ?? UIColor.white

        if let view = view {
            var frame = view.frame

            frame.origin.y = (indexPath as NSIndexPath).row == 0 ? (self.frame.height / 2) - (rowHeight / 2) : 0.0
            view.frame = frame

            pickerViewCell.customView = view

            guard let customView = pickerViewCell.customView else { return UITableViewCell() }

            pickerViewCell.contentView.addSubview(customView)

        } else {
            // As the first row have a different size to fit in the middle of the PickerView and rows below, the titleLabel position must be adjusted.
            let centerY = (indexPath as NSIndexPath).row == 0 ? (frame.height / 2) - (rowHeight / 2) : 0.0

            pickerViewCell.titleLabel.frame = CGRect(x: 0.0, y: centerY, width: frame.width, height: rowHeight)

            pickerViewCell.contentView.addSubview(pickerViewCell.titleLabel)
            pickerViewCell.titleLabel.backgroundColor = UIColor.clear
            pickerViewCell.titleLabel.text = dataSource?.pickerView(self, titleForRow: indexForRow((indexPath as NSIndexPath).row))

            delegate?.pickerView?(self, styleForLabel: pickerViewCell.titleLabel, highlighted: (indexPath as NSIndexPath).row == indexOfSelectedRow)
        }

        return pickerViewCell
    }
}
